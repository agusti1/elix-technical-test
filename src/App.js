import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import AssetsScreen from './components/AssetsScreen';
import EntitiesScreen from "./components/EntitiesScreen";
import Header from './components/Header';


const app = () => {
    return (
        <Router>
            <div className='App'>
            <Header/>
            <div className='container-home'>
                <Route path="/" exact component={AssetsScreen} />
            </div>
            <Route path="/entities/:id" component={EntitiesScreen} />
        {/*<Route path="/series" component={Series} />*/}
        {/*<div className='container-info'>*/}
        {/*  <Route path="/info/:media_type/:media_id" component={Info} />*/}
        {/*</div>*/}
            </div>
        </Router>
    );
};

export default app;
