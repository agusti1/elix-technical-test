import React, {Component} from 'react';
import Axios from 'axios';
import '../css/TableScreens.css';
import AssetsTable from './tableComponents/AssetsTable';


export default class EntitiesScreen extends Component {
    state = {
        entities: [],
        loading: true,
    };

    componentDidMount() {
        let assetId = this.props.match.params.id;

        Axios.get('/elixos/entities')
            .then(res => {
                for(let i = 0; i<res.data.entities.length; i++){
                    if(res.data.entities[i].id_asset === JSON.parse(assetId)){
                        this.setState(prevState => ({
                            entities: [...prevState.entities, res.data.entities[i]],
                            loading: false,
                        }))
                    }
                }
            })
    }


    handleContextClick(){
        console.log('hello')
    }


    render() {

        //---- SET HEAD AND ROWS FOR TABLE ----//
        let {entities} = this.state;
        let rows = [];
        const headings =  [ 'ID', 'ENTRY USE', 'ENTRY BUILDING', 'ENTRY VIEW', 'ENTRY CODE', 'ENTRY NAME', 'VACANCY STATUS NEGOTIATION', 'VACANCY EXPECTED DATE', 'VACANCY AGENT', 'VACANCY COMPENSATION PAYMENT FIRST DATE'];
        if(entities.length > 0){
            for(let i = 0; i < entities.length; i++){
                rows.push([entities[i].id, entities[i].t_entry_use, entities[i].t_entry_building, entities[i].t_entry_view, entities[i].t_entry_code, entities[i].t_entry_name_official, entities[i].t_bp_vacancy_status_negotiation, entities[i].d_bp_vacancy_expected_date, entities[i].t_bp_vacancy_agent, entities[i].d_bp_vacancy_compensation_payment_first_date,])
            }
        }

        if (this.state.loading) {
            return (
                <div className='middle'>
                    <p>Loading...</p>
                </div>
            )
        } else {
            return (
                <div className='tabla'>
                    <AssetsTable headings={headings} rows={rows} />
                </div>
            )
        }
    }
}