import React, {Component} from 'react';
import Axios from 'axios';
import {Link} from 'react-router-dom';
import iconGo from '../img/icon_go.png';
import '../css/TableScreens.css';
import AssetsTable from './tableComponents/AssetsTable';

export default class AssetsScreen extends Component {
    state = {
        assets: [],
        loading: true,
    };

    componentDidMount() {
        Axios.get('/elixos/assets')
            .then(res => {
                this.setState({
                    assets : res.data.assets,
                    loading: false,
                });
            })
    }

    //---- create entities button ----//
    getButton(id){
        return <Link to={`/entities/${id}`}>
            <button>
                <img className='imgGo' src={iconGo} alt=""/>
            </button>
        </Link>
    }


    render() {

        //---- SET HEAD AND ROWS FOR TABLE ----//
        let {assets} = this.state;
        let rows = [];
        const headings = ['ID', 'STREET', 'NUMBER', 'CITY', 'CODE', 'ENTITIES'];
        if(assets.length > 0){
            for(let i = 0; i < assets.length; i++){
                rows.push([assets[i].id, assets[i].t_street_name, assets[i].n_number, assets[i].t_city, assets[i].t_code, this.getButton(this.state.assets[i].id)])
            }
        }




        if(this.state.loading){
            return(
                <div className='middle'>
                    <p>Loading...</p>
                </div>
            )
        }else{
            return(
                <div className='tabla'>
                    <AssetsTable headings={headings} rows={rows} />
                </div>
            )
        }
    }
}