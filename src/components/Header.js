import React from 'react'
import { Link } from 'react-router-dom';
import '../css/Header.css';

const header = () => {
    return (
        <header>
            <div className="logo">
                <Link to="/" >Home</Link>
            </div>
        </header>
    )
};
export default header;
